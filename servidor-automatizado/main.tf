resource "aws_security_group" "permitir_ssh_http" {
  name = "permitir_ssh"
  description = "Permite SSH e HTTP na instancia EC2"

  ingress{
    cidr_blocks = [ "0.0.0.0/0" ]
    ipv6_cidr_blocks = [ "::/0" ]
    from_port = 0
    to_port = 0
    protocol = "-1"
  }
  egress{
    cidr_blocks = [ "0.0.0.0/0" ]
    ipv6_cidr_blocks = [ "::/0" ]
    from_port = 0
    to_port = 0
    protocol = "-1"
  }

  tags = {
    Name = "permitir_ssh_e_http"
  }
}

resource "aws_instance" "servidor" {
  ami = "ami-0e83be366243f524a"
  instance_type = "t2.micro"
  security_groups = [aws_security_group.permitir_ssh_http.name]

  user_data = file("install.sh")

  tags = {
    Name = "Jumpt The Cat"
  }
}

output "public_ip" {
  value = aws_instance.servidor.public_ip
}