#!/bin/bash

# Nome do arquivo HTML
html_file="/var/www/html/index.html"

# Configs para Notificacoes sobre o processo
TOKEN="6307387198:AAHq-jbtdVxBSDqXw9xjuJEo3AYZGxvh5Fo"
CHAT_ID="1029505131"
TEMPO_EM_SEGUNDOS=120

# Conteúdo HTML a ser inserido no arquivo
html_content="<html>
<head>
    <title>Minha Pagina Web</title>
</head>
<body>
    <h1>Ola, Alunos!</h1>
    <p>Este e um arquivo HTML simples servido por um servidor Apache em uma instância EC2 de forma automatizada com IaC.</p>
</body>
</html>"

curl -s -X POST "https://api.telegram.org/bot$TOKEN/sendMessage" -d "chat_id=$CHAT_ID" -d "text=RUNNING SERVER, STARTED UPDATE SYSTEM."

# Atualizar o sistema
sudo apt-get update
sudo apt-get upgrade -y

# Instalar o servidor web Apache
sudo apt-get install apache2 -y

# Criar o arquivo HTML
echo "$html_content" | sudo tee "$html_file" > /dev/null

curl -s -X POST "https://api.telegram.org/bot$TOKEN/sendMessage" -d "chat_id=$CHAT_ID" -d "text=STARTED APACHE2 SERVER."

# Configurar o servidor Apache para iniciar automaticamente na inicialização
sudo systemctl enable apache2

# Iniciar o servidor Apache
sudo systemctl start apache2

# Abrir a porta 80 no firewall (permitir tráfego HTTP)
sudo ufw allow 80

curl -s -X POST "https://api.telegram.org/bot$TOKEN/sendMessage" -d "chat_id=$CHAT_ID" -d "text=SERVER HAS CONFIGURED."

sleep $TEMPO_EM_SEGUNDOS

curl -s -X POST "https://api.telegram.org/bot$TOKEN/sendMessage" -d "chat_id=$CHAT_ID" -d "text=SERVER ONLINE."